package com.popular.evergreen.morningquotes

interface AppInterfaces {
    fun loadItem()
    fun loadSplashScreen()
    fun loadStartScreen()
    fun loadImageTopics()
    fun loadMenus()
    fun loadTestModeScreen()
    fun loadBookMarkMenu()
    fun loadBookMarkItem()
    fun loadPrivacyPolicy()


}