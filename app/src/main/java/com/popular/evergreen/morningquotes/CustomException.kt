package com.popular.evergreen.morningquotes

class CustomException(message: String) : Exception(message)
